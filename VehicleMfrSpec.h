/**
 * Project Vehicle_Mgt
 */
#include "VehicleSpec.h"
#include "Manufacturer.h"
#include "Vehicle.h"

#ifndef _VEHICLEMFRSPEC_H
#define _VEHICLEMFRSPEC_H

class VehicleMfrSpec: public VehicleSpec {
public: 
    VehicleMfrSpec(Manufacturer mfr) { m_mfr = mfr; }

    /**
     * @param vehicle
     */
    virtual bool isSatisfied(const Vehicle& vehicle) const
    {
        auto mfr = vehicle.getManufacturer();
        return (mfr.name == m_mfr.name) && (mfr.country == m_mfr.country);
    }
protected: 
    Manufacturer m_mfr;
};

#endif //_VEHICLEMFRSPEC_H