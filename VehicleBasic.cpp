/**
 * Project Vehicle_Mgt
 */

#include "VehicleBasic.h"
#include <sstream>

/**
 * VehicleBasic implementation
 */

VehicleBasic::VehicleBasic(string vin, Manufacturer mfr, string model, int year, string color)
{
    Vehicle::setVin(vin);
    Vehicle::setManufacturer(mfr);
    Vehicle::setModel(model);
    Vehicle::setYear(year);
    Vehicle::setColor(color);
    setDescription("");
}

VehicleBasic* VehicleBasic::clone() const
{
    return new VehicleBasic(*this);
}

void VehicleBasic::setManufacturer(Manufacturer mfr)
{
    Vehicle::setManufacturer(mfr);
}

void VehicleBasic::setYear(int year)
{
    Vehicle::setYear(year);
}

void VehicleBasic::setColor(string color)
{
    Vehicle::setColor(color);
}

void VehicleBasic::setVin(string vin)
{
    Vehicle::setVin(vin);
}

void VehicleBasic::setDescription(string desc)
{
    if (desc.empty()) // for testing purposes
    {
        stringstream stream;
        Manufacturer mfr = getManufacturer();
        stream << getVin() << ": " << getColor() << " " << getYear() << " " << getModel() <<
            " mfd. by " << mfr.name << " in " << mfr.country << endl;
        Vehicle::setDescription(stream.str());
    }
    else Vehicle::setDescription(desc);
}
