/**
 * Project Vehicle_Mgt
 */


#include "VehicleInventory.h"
#include "VehicleYearSpec.h"
#include "VehicleMfrSpec.h"
#include "VehicleSpec.h"
#include "VehicleSpecs.h"

/**
 * VehicleInventory implementation
 */

E_RETCODE VehicleInventory::addVehicle(const Vehicle & vehicle)
{
    size_t index = findVehicle(vehicle);
    if (index != -1)
    {
        cout << "ERROR: Vehicle already exists! Vehicle description: " << 
            vehicleList[index]->getDescription() << endl;
        return E_ERROR;
    }
    else vehicleList.push_back(vehicle.clone());

    return E_OK;
}

E_RETCODE VehicleInventory::removeVehicle(const Vehicle & vehicle)
{
    size_t index = findVehicle(vehicle);
    if (index == -1)
    {
        cout << "ERROR: Vehicle doesn't exist!" << endl;
        return E_ERROR;
    }
    vehicleList.erase(vehicleList.begin() + index);
    return E_OK;
}

bool VehicleInventory::hasVehicle(const Vehicle& vehicle)
{
    return findVehicle(vehicle) != -1;
}

// TODO add variadic template to allow filtering by multiple specs
VehicleInventory VehicleInventory::getVehicles() const
{
    VehicleInventory newInventory = VehicleInventory();
    for (Vehicle* v : vehicleList)
    {
        newInventory.vehicleList.push_back(v->clone());
    }
    return newInventory;
}

VehicleInventory VehicleInventory::getVehiclesByYear(int year) const
{
    VehicleInventory newInventory = VehicleInventory();
    VehicleYearSpec yearSpec = VehicleYearSpec(year);
    for (Vehicle* v : vehicleList)
    {
        if (yearSpec.isSatisfied(*v))
        {
            newInventory.vehicleList.push_back(v->clone());
        }
    }
    return newInventory;
}

VehicleInventory VehicleInventory::getVehiclesByMfr(Manufacturer mfr) const
{
    VehicleInventory newInventory = VehicleInventory();
    VehicleMfrSpec mfrSpec = VehicleMfrSpec(mfr);
    for (Vehicle* v : vehicleList)
    {
        if (mfrSpec.isSatisfied(*v))
        {
            newInventory.vehicleList.push_back(v->clone());
        }
    }
    return newInventory;
}

vector<Vehicle*> VehicleInventory::getVehiclesAsArray() const
{
    return vector<Vehicle*>(vehicleList);
}

size_t VehicleInventory::findVehicle(const Vehicle& vehicle)
{
    // Checks to see if vin matches any in inventory
    // (Could use UUIDs instead to make sure another vehicle with same
    //  information matches correctly)
    for (size_t i = 0; i < vehicleList.size(); i++) 
    {
        if (vehicleList[i]->getVin() == vehicle.getVin())
            return i;
    }
    return -1;
}

/*
UUID VehicleInventory::getVehicleId(const Vehicle & vehicle)
{
    return UUID();
}
*/

ostream & operator<<(ostream & os, const VehicleInventory & inv)
{
    os << "[\n";
    for (Vehicle* v : inv.vehicleList)
    {
        os << "\t" << v->getDescription() << "\n";
    }
    os << "]" << endl;
    return os;
}
