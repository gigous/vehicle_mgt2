/**
 * Project Vehicle_Mgt
 */

#include "VehicleSpec.h"

#ifndef _VEHICLEYEARSPEC_H
#define _VEHICLEYEARSPEC_H

class VehicleYearSpec: public VehicleSpec {
public: 
    VehicleYearSpec(int year) { m_year = year; }
    /**
     * @param const Vehicle&
     */
    virtual bool isSatisfied(const Vehicle& vehicle) const
    {
        return vehicle.getYear() == m_year;
    }
protected: 
    int m_year;
};

#endif //_VEHICLEYEARSPEC_H