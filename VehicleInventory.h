/**
 * Project Vehicle_Mgt
 */

#include <vector>
#include "Manufacturer.h"
#include "windows.h"
#include "Vehicle.h"

using namespace std;

#ifndef _VEHICLEINVENTORY_H
#define _VEHICLEINVENTORY_H

enum E_RETCODE
{
    E_OK, E_ERROR
};

// Assuming this does not need a high degree of expansion and flexibility
// As in, using arrays of different kinds of vehicle inventories
// This is just to demonstrate a working solution
class VehicleInventory {
public: 
    VehicleInventory() 
    {
        vehicleList = vector<Vehicle*>();
    }
    
    /**
     * @param const Vehicle&
     */
    E_RETCODE addVehicle(const Vehicle& vehicle);
    
    /**
     * @param 
     */
    E_RETCODE removeVehicle(const Vehicle& vehicle);
    
    bool hasVehicle(const Vehicle& vehicle);

    /**
     * @param 
     */
    virtual VehicleInventory getVehicles() const;
    
    /**
     * @param year
     */
    virtual VehicleInventory getVehiclesByYear(int year) const;
    
    /**
     * @param mfr
     */
    virtual VehicleInventory getVehiclesByMfr(Manufacturer mfr) const;

    // For testing purposes
    vector<Vehicle*> getVehiclesAsArray() const;

    virtual size_t size() const { return vehicleList.size(); }
protected: 
    size_t findVehicle(const Vehicle& vehicle);

    vector<Vehicle*> vehicleList;
    //UUID getVehicleId(const Vehicle& vehicle);

private: 
    /**
     * @param os
     * @param vehicle
     */
    friend ostream& operator<<(ostream& os, const VehicleInventory& inv);
};

#endif //_VEHICLEINVENTORY_H