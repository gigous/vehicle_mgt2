# Vehicle Management System (in C++)

This code is written in C++ to model a simple vehicle management system.

The master branch is where you'll find a working solution. The code
here has been tested, and basic requirements were met, including creating
a vehicle inventory which can add, remove, filter, and show stored vehicles on a console.
Tests are found in vehicle_mgt_test.cpp. The system was formulated using UML, and the diagram
found in extra (Main.png) was used as a foundation for defining the code.

This sample was written in Windows 10 using Visual Studio 2019. It is recommended
any user of this code download the Desktop Development with C++ workload for
Visual Studio 2019. Please see [here](https://docs.microsoft.com/en-us/visualstudio/test/how-to-use-google-test-for-cpp?view=vs-2019)
for instructions.

~~**Please see the [abstractFactory branch](https://gitlab.com/gigous/vehicle_mgt2/tree/abstractFactory) for the most up-to-date code.**~~ In this branch, each class has a brief description for what its purpose is. The branch is to continue development on implementing an Abstract Factory design pattern to be able to more easily extend the Vehicle base class.

The [redo](https://gitlab.com/gigous/vehicle_mgt2/tree/redo) branch contains a rework of the vehicle management system based on feedback from others. The difference between this and the other code is that it features a simple console application for adding and removing vehicles dynamically. There are three different concrete vehicles: Tesla, BMW, and Ford. The creation of these types of vehicles is done by a static factory method in the VehicleFactory class. This class can be modified to allow for more models of the various vehicle manufacturers, or the mentioned classes can be extended for even more flexibility.

Commands for the customer application:

- **a** - Add vehicle
- **r** - Remove vehicle
- **p** - Print inventory
- **y** - Print inventory filtered by vehicle year
- **m** - Print inventory filtered by vehicle manufacturer
- **q** - Quit
