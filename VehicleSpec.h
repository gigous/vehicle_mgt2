/**
 * Project Vehicle_Mgt
 */

#include "Vehicle.h"

#ifndef _VEHICLESPEC_H
#define _VEHICLESPEC_H

class VehicleSpec {
public:
    virtual ~VehicleSpec() {}

    /**
    * @param vehicle
    */
    virtual bool isSatisfied(const Vehicle& vehicle) const = 0;

};

#endif //_VEHICLESPEC_H