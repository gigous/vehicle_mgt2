/**
 * Project Vehicle_Mgt
 */

#include "Vehicle.h"
#include "Manufacturer.h"

#ifndef _VEHICLEBASIC_H
#define _VEHICLEBASIC_H

class VehicleBasic: public Vehicle {
    friend ostream& operator<<(ostream& os, const VehicleBasic& vehicle)
    {
        os << vehicle.getDescription();
        return os;
    }

public:
    VehicleBasic() {}
    /**
     * @param vin
     * @param mfr
     * @param model
     * @param year
     * @param color
     */
    VehicleBasic(string vin, Manufacturer mfr, string model, int year, string color);
    
    virtual ~VehicleBasic() {};

    virtual VehicleBasic* clone() const;

    /**
    * @param string
    */
    virtual string getEngine() const
    {
        return "a good one";
    }

    /**
    *
    */
    virtual double getEstimatedCost() const
    {
        return 0.0;    
    }
protected: 
    
    /**
     * @param Manufacturer
     */
    virtual void setManufacturer(Manufacturer mfr);
    
    /**
     * @param int
     */
    virtual void setYear(int year);
    
    /**
     * @param string
     */
    virtual void setColor(string color);
    
    /**
     * @param string
     */
    virtual void setVin(string vin);
    
    /**
     * @param string
     */
    virtual void setDescription(string desc);
private: 

};

#endif //_VEHICLEBASIC_H