#include <iostream>
#include "VehicleBasic.h"
#include "VehicleInventory.h"

using namespace std;

int main() {
    Manufacturer mfr = {"BMW", "Germany"};
    
    VehicleBasic vehicle = VehicleBasic("5513F", mfr, "M2", 2015, "white");
    VehicleBasic vehicle2 = VehicleBasic("5513612", mfr, "M5", 2018, "silver");
    string i;
    
    cout << vehicle << endl;
    VehicleInventory vi = VehicleInventory();
    vi.addVehicle(vehicle);
    vi.addVehicle(vehicle2);

    cout << vi << endl;

    cin >> i; // Pause terminal
    
    return 0;
}