#include "pch.h"
#include "../VehicleInventory.h"
#include "../VehicleInventory.cpp" // Not ideal, in future use library
#include "../VehicleBasic.h"
#include "../VehicleBasic.cpp" // Not ideal, in future use library

TEST(GTest, GoogleTestWorks)
{
    ASSERT_EQ(1, 1);
    ASSERT_TRUE(true);
}

class VehicleBasicTest : public ::testing::Test
{
protected:
    void SetUp() override {
        vb = VehicleBasic(myVin, myMfr, myModel, myYear, myColor);
    }
    VehicleBasic vb;
    const string myVin = "5513F";
    const Manufacturer myMfr = { "BMW", "Germany" };
    const string myModel = "M2";
    const int myYear = 2005;
    const string myColor = "white";

};

class VehicleInventoryTest : public ::testing::Test
{
protected:
    void SetUp() override {
        vi = VehicleInventory();
        Manufacturer mfr = { "BMW", "Germany" };
        Manufacturer mfrAnother = { "Tesla", "United States" };
        vb = VehicleBasic("EET5", mfr, "M2", 2005, "white");
        vbOther = VehicleBasic("EET43", mfr, "M2", 2015, "white");
        vbAnother = VehicleBasic("ELON", mfrAnother, "Model S", 2017, "black");
    }
    VehicleInventory vi;
    VehicleBasic vb, vbOther, vbAnother;
};

TEST(VehicleInventoryInitTest, CreateInventory) {
    VehicleInventory vi = VehicleInventory();
    SUCCEED();
}


TEST(VehicleInitTest, CreateVehicle) {
    Manufacturer mfr = { "BMW", "Germany" };
    VehicleBasic vb("5513F", mfr, "M2", 2015, "white");
    SUCCEED();
}

/* Using test fixture VehicleBasicTest */

TEST_F(VehicleBasicTest, VehicleVinSet)
{
    ASSERT_EQ(vb.getVin(), myVin);
}

TEST_F(VehicleBasicTest, VehicleMfrSet)
{
    ASSERT_EQ(vb.getManufacturer().name, myMfr.name);
    ASSERT_EQ(vb.getManufacturer().country, myMfr.country);
}

TEST_F(VehicleBasicTest, VehicleModelSet)
{
    ASSERT_EQ(vb.getModel(), myModel);
}

TEST_F(VehicleBasicTest, VehicleYearSet)
{
    ASSERT_EQ(vb.getYear(), myYear);
}

TEST_F(VehicleBasicTest, VehicleColorSet)
{
    ASSERT_EQ(vb.getColor(), myColor);
}

/* Using test fixture VehicleInventoryTest */

TEST_F(VehicleInventoryTest, AddVehicle)
{
    EXPECT_EQ(vi.addVehicle(vb), E_OK);
    ASSERT_TRUE(vi.hasVehicle(vb));
}

TEST_F(VehicleInventoryTest, AddVehicleAgain)
{
    EXPECT_EQ(vi.addVehicle(vb), E_OK);
    EXPECT_TRUE(vi.hasVehicle(vb));
    ASSERT_EQ(vi.addVehicle(vb), E_ERROR);
}

TEST_F(VehicleInventoryTest, PrintInventory)
{
    EXPECT_EQ(vi.addVehicle(vb), E_OK);
    EXPECT_EQ(vi.addVehicle(vbOther), E_OK);
    cout << vi << endl;
    SUCCEED();
}

TEST_F(VehicleInventoryTest, RemoveVehicle)
{
    EXPECT_EQ(vi.addVehicle(vb), E_OK);
    EXPECT_TRUE(vi.hasVehicle(vb));
    EXPECT_EQ(vi.removeVehicle(vb), E_OK);
    ASSERT_FALSE(vi.hasVehicle(vb));
}

TEST_F(VehicleInventoryTest, RemoveInvalidVehicle)
{
    EXPECT_EQ(vi.addVehicle(vb), E_OK);
    ASSERT_EQ(vi.removeVehicle(vbOther), E_ERROR);
}

TEST_F(VehicleInventoryTest, FilterByYear)
{
    EXPECT_EQ(vi.addVehicle(vb), E_OK);
    EXPECT_EQ(vi.addVehicle(vbOther), E_OK);
    EXPECT_EQ(vi.addVehicle(vbAnother), E_OK);
    auto vi2017 = vi.getVehiclesByYear(2017);
    EXPECT_EQ(vi2017.getVehiclesAsArray()[0]->getManufacturer().name, "Tesla");
    ASSERT_EQ(vi2017.size(), 1);
}

TEST_F(VehicleInventoryTest, FilterByMfr)
{
    EXPECT_EQ(vi.addVehicle(vb), E_OK);
    EXPECT_EQ(vi.addVehicle(vbOther), E_OK);
    EXPECT_EQ(vi.addVehicle(vbAnother), E_OK);
    Manufacturer bmw = { "BMW", "Germany" };
    auto viBmw = vi.getVehiclesByMfr(bmw);
    cout << viBmw << endl;
    ASSERT_EQ(viBmw.size(), 2);
}