/**
 * Project Vehicle_Mgt
 */


#ifndef _VEHICLESPECS_H
#define _VEHICLESPECS_H

#include "VehicleSpec.h"


class VehicleSpecs: public VehicleSpec {
public: 
    
    /**
     * @param const Vehicle&
     */
    virtual bool isSatisfied(const Vehicle& vehicle) const
    {
        return firstSpec.isSatisfied(vehicle) && secondSpec.isSatisfied(vehicle);
    }
    
    /**
     * @param const VehicleSpec&
     * @param const VehicleSpec&
     */
    VehicleSpecs(const VehicleSpec& first, const VehicleSpec& second)
        : firstSpec(first), secondSpec(second) {}
protected: 
    const VehicleSpec& firstSpec;
    const VehicleSpec& secondSpec;
};

#endif //_VEHICLESPECS_H