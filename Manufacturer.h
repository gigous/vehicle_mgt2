/**
 * Project Vehicle_Mgt
 */
#include <string>

using namespace std;

#ifndef _MANUFACTURER_H
#define _MANUFACTURER_H

struct Manufacturer {
public: 
    string name;
    string country;
    bool operator==(const Manufacturer& rhs)
    {
        return (name == rhs.name && country == rhs.country);
    }
};

#endif //_MANUFACTURER_H