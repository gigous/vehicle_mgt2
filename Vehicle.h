/**
 * Project Vehicle_Mgt
 */

#include <string>
#include "Manufacturer.h"
#include <random>
#include <sstream>
#include <iostream>

using namespace std;

#ifndef _VEHICLE_H
#define _VEHICLE_H

class VehicleInventory;

class Vehicle {
public:
    
    /**
     * @param string
     */
    virtual string getEngine() const = 0;
    
    /**
    *
    */
    virtual double getEstimatedCost() const = 0;

    /**
    *
    */
    virtual Manufacturer getManufacturer() const { return m_mfr; }

    /**
    *
    */
    virtual string getModel() const { return m_model; }

    /**
    *
    */
    virtual int getYear() const { return m_year; }

    /**
    *
    */
    virtual string getColor() const { return m_color; }

    /**
    *
    */
    virtual string getVin() const { return m_vin; }

    /**
    *
    */
    virtual string getDescription() const { return m_desc; }

    virtual Vehicle* clone() const = 0;
protected:
    
    Vehicle() : m_vin(""), m_mfr({ "","" }), m_model(""), m_year(0), m_color("")
    {
        /*
        if (RPC_S_OK != UuidCreate(&m_id))
            cout << "Error: Couldn't create uuid" << endl;
        */
    }
    
    virtual ~Vehicle() = default;
    
    /**
     * @param mfr
     */
    virtual void setManufacturer(Manufacturer mfr) { m_mfr = mfr; }
    
    /**
     * @param model
     */
    virtual void setModel(string model) { m_model = model; }

    /**
     * @param year
     */
    virtual void setYear(int year) { m_year = year; }
    
    /**
     * @param color
     */
    virtual void setColor(string color) { m_color = color; }
    
    /**
     * @param vin
     */
    virtual void setVin(string vin) { m_vin = vin; }
    
    /**
     * @param desc
     */
    virtual void setDescription(string desc) 
    {
        m_desc = desc;
    }
private: 
    string m_vin;
    Manufacturer m_mfr;
    string m_model;
    int m_year;
    string m_color;
    string m_desc;
    //UUID m_id;
    
    /**
     *
     */
    //UUID getID() const { return m_id; }
    //friend VehicleInventory; // Tried giving only getVehicleId() access but didn't work as planned :/
};

#endif //_VEHICLE_H